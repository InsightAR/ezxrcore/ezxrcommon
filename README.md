# EZXR Common

EZXR Common 是ezxr各个模块共用模块的unity package manager。包含单例模式、简单的Http、Log系统、左右手坐标系变换等模块

## 对应版本

| Unity Version | EZXR Common |
| ------------- | ----------- |
| 2021.3        | 1.1.0       |

## Release Note

### 1.1.0

1. CoordinateTransformation cv右手坐标系到unity坐标系之间的坐标变换
2. 单例模式
3. ECLog
4. Network相关的http和ScoketIO 
