﻿namespace EzxrCore.Network.SocketIOClient
{
    public enum ServerCloseReason
    {
        ClosedByClient,
        ClosedByServer,
        Aborted
    }
}
