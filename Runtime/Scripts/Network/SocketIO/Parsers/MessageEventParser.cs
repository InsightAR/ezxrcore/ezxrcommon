﻿using System.Text.RegularExpressions;
using System.Threading.Tasks;
using EzxrCore.Network.SocketIOClient.Arguments;

namespace EzxrCore.Network.SocketIOClient.Parsers
{
    class MessageEventParser : IParser
    {
        public Task ParseAsync(ResponseTextParser rtp)
        {
            //Console.WriteLine("text: " + rtp.Text);
            //Change
            var regex = new Regex($@"[\d\w-]*{rtp.Namespace}\d*\[""([*\s\w-/_]+)"",([\s\S]*)\]$");
            if (regex.IsMatch(rtp.Text))
            {
                var groups = regex.Match(rtp.Text).Groups;

                string eventName = groups[1].Value;

                var args = new ResponseArgs
                {
                    Text = groups[2].Value,
                    RawText = rtp.Text
                };
                if (rtp.Socket.EventHandlers.ContainsKey(eventName))
                {
                    var handler = rtp.Socket.EventHandlers[eventName];
                    handler(args);
                }
                else
                {
                    rtp.Socket.InvokeUnhandledEvent(eventName, args);
                }
                rtp.Socket.InvokeReceivedEvent(eventName, args);
                return Task.CompletedTask;
            }
            else
            {
                rtp.Parser = new MessageAckParser();
                return rtp.ParseAsync();
            }
        }
    }
}
