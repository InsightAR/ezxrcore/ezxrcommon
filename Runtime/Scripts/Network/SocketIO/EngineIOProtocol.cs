﻿namespace EzxrCore.Network.SocketIOClient
{
    public enum EngineIOProtocol
    {
        Open,
        Close,
        Ping,
        Pong,
        Message,
        Upgrade,
        Noop
    }
}
