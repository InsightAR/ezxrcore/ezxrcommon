﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using UnityEngine.Networking;

namespace EzxrCore.Network.Http
 {
	 /**
     * \~english
     * Http request body
     * 
     * \~chinese
     * Http请求体
     */
	
	public class RequestBody {
		private string contentType;
		private byte[] body;

		/**
         * \~english
         * @brief Construction method of RequestBody
         *
         * @param contentType Request content type
         *
         * @param str Request body data
         * 
         * \~chinese
         * @brief RequestBody的构造方法
         *
         * @param contentType 请求的内容类型
         *
         * @param str 请求体数据
         * 
         */
		public RequestBody(string contentType, string str) {
			this.contentType = contentType;
			this.body = System.Text.Encoding.UTF8.GetBytes(str);
			// this.body = body;
		}

		/**
         * \~english
         * @brief Construct a request body
         *
         * @param value A string
         * 
         * \~chinese
         * @brief 构造一个请求体
         *
         * @param value 一个字符串
		 * 
         */
		public static RequestBody From(string value) {
			byte[] bodyRaw = Encoding.UTF8.GetBytes(value.ToCharArray());
			return new RequestBody ("application/x-www-form-urlencoded", value);
		}

		/**
         * \~english
         * @brief Construct a request body
         *
         * @param value An object of type <T>
         * 
         * \~chinese
         * @brief 构造一个请求体
         *
         * @param value 一个<T>类型的对象
		 * 
         */
		public static RequestBody From<T>(T value) {
               string json = JsonUtility.ToJson(value);
			byte[] bodyRaw = Encoding.UTF8.GetBytes(json.ToCharArray());
			return new RequestBody ("application/json", Encoding.UTF8.GetString(bodyRaw));
		}
			
		/**
         * \~english
         * @brief Construct a request body
         *
         * @param value A FormData object
         * 
         * \~chinese
         * @brief 构造一个请求体
         *
         * @param value 一个FormData对象
		 * 
         */
		public static RequestBody From(FormData form) {
			// https://answers.unity.com/questions/1354080/unitywebrequestpost-and-multipartform-data-not-for.html

			List<IMultipartFormSection> formData = form.MultipartForm ();

			// generate a boundary then convert the form to byte[]
			byte[] boundary = UnityWebRequest.GenerateBoundary();
			byte[] formSections = UnityWebRequest.SerializeFormSections(formData, boundary);
			// my termination string consisting of CRLF--{boundary}--
			byte[] terminate = Encoding.UTF8.GetBytes(String.Concat("\r\n--", Encoding.UTF8.GetString(boundary), "--"));
			// Make complete body from the two byte arrays
			byte[] bodyRaw = new byte[formSections.Length + terminate.Length];
			Buffer.BlockCopy(formSections, 0, bodyRaw, 0, formSections.Length);
			Buffer.BlockCopy(terminate, 0, bodyRaw, formSections.Length, terminate.Length);
			// Set the content type
			string contentType = String.Concat("multipart/form-data; boundary=", Encoding.UTF8.GetString(boundary));
			return new RequestBody (contentType, Encoding.UTF8.GetString(bodyRaw));
		}

		[System.Obsolete("WWWForm is obsolete. Use List<IMultipartFormSection> instead")]
		public static RequestBody From(WWWForm formData) {
			return new RequestBody ("application/x-www-form-urlencoded", Encoding.UTF8.GetString(formData.data));
		}

		/**
         * \~english
         * @brief Get the requested content type
         *
         * \~chinese
         * @brief 获取请求的内容类型
         *
         */
		public string ContentType() {
			return contentType;
		}

		/**
         * \~english
         * @brief Get request body, the return value is the byte data of the request body.
         *
         * \~chinese
         * @brief 获取请求体，返回值为请求体的字节数据
         *
         */
		public byte[] Body() {
			return body;
		}
	}
}
