﻿using System.Collections.Generic;
using UnityEngine.Networking;

namespace EzxrCore.Network.Http
{
	/**
     * \~english
     * Construct the data structure of RequestBody
     * 
     * \~chinese
     * 构造RequestBody的数据结构
     */
	public class FormData {

		private List<IMultipartFormSection> formData;

		public FormData() {
			formData = new List<IMultipartFormSection> ();
		}

		/**
         * \~english
         * @brief Add key-value pairs
         *
         * @param name key
		 * 
         * @param value value
		 *
		 * @return STXR.Network.Http.FormData
         * 
         * \~chinese
         * @brief 添加键值对
         *
         * @param name key值
		 * 
         * @param value value
		 *
		 * @return STXR.Network.Http.FormData
		 * 
         */
		public FormData AddField(string name, string value) {
			formData.Add (new MultipartFormDataSection (name, value));
			return this;
		}

		/**
         * \~english
         * @brief Add a fragment of a famous file
         *
         * @param name The name of the file fragment
		 * 
         * @param value a byte array
		 *
		 * @return STXR.Network.Http.FormData
         * 
         * \~chinese
         * @brief 添加有名文件片段
         *
         * @param name 文件片段的名字
		 * 
         * @param value 字节数组
		 *
		 * @return STXR.Network.Http.FormData
		 * 
         */
		public FormData AddFile(string name, byte[] data) {
			formData.Add (new MultipartFormFileSection (name, data));
			return this;
		}

		/**
         * \~english
         * @brief Add a fragment of a famous file
         *
         * @param name File name after upload
		 * 
         * @param value a byte array
		 *
		 * @return STXR.Network.Http.FormData
         * 
         * \~chinese
         * @brief 添加有名文件片段
         *
         * @param name 上传后的文件名字
		 * 
         * @param value 字节数组
		 *
		 * @return STXR.Network.Http.FormData
		 * 
         */
		public FormData AddFile(string name, byte[] data, string fileName, string contentType) {
			formData.Add (new MultipartFormFileSection (name, data, fileName, contentType));
			return this;
		}

		public List<IMultipartFormSection> MultipartForm() {
			return formData;
		}
	}
}
