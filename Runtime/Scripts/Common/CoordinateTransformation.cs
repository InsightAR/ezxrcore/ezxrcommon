using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace EzxrCore.Common
{
    public class CoordinateTransformation
    {
        public static Matrix4x4 ConvertCVPoseToUnityPose_c2w(Matrix4x4 cvPose)
        {
            Matrix4x4 unityPose = Matrix4x4.identity;
            unityPose.m00 = cvPose.m00; unityPose.m01 = -cvPose.m01; unityPose.m02 = cvPose.m02; unityPose.m03 = cvPose.m03;
            unityPose.m10 = cvPose.m20; unityPose.m11 = -cvPose.m21; unityPose.m12 = cvPose.m22; unityPose.m13 = cvPose.m23;
            unityPose.m20 = cvPose.m10; unityPose.m21 = -cvPose.m11; unityPose.m22 = cvPose.m12; unityPose.m23 = cvPose.m13;
            unityPose.m30 = 0; unityPose.m31 = 0; unityPose.m32 = 0; unityPose.m33 = 1;
            return unityPose;
        }

        public static Matrix4x4 ConvertCVPoseToUnityPose_c2w(float[] poses)
        {
            Matrix4x4 cvPose = Matrix4x4.identity;
            cvPose.m00 = poses[0]; cvPose.m01 = poses[1]; cvPose.m02 = poses[2]; cvPose.m03 = poses[3];
            cvPose.m10 = poses[4]; cvPose.m11 = poses[5]; cvPose.m12 = poses[6]; cvPose.m13 = poses[7];
            cvPose.m20 = poses[8]; cvPose.m21 = poses[9]; cvPose.m22 = poses[10]; cvPose.m23 = poses[11];
            cvPose.m30 = poses[12]; cvPose.m31 = poses[13]; cvPose.m32 = poses[14]; cvPose.m33 = poses[15];

            Matrix4x4 unityPose = ConvertCVPoseToUnityPose_c2w(cvPose);
            return unityPose;
        }

        public static Matrix4x4 ConvertUnityPoseToCvPose_c2w(Matrix4x4 unityPose)
        {
            Matrix4x4 cvPose = Matrix4x4.identity;

            cvPose.m00 = unityPose.m00; cvPose.m01 = -unityPose.m01; cvPose.m02 = unityPose.m02; cvPose.m03 = unityPose.m03;
            cvPose.m10 = unityPose.m20; cvPose.m11 = -unityPose.m21; cvPose.m12 = unityPose.m22; cvPose.m13 = unityPose.m23;
            cvPose.m20 = unityPose.m10; cvPose.m21 = -unityPose.m11; cvPose.m22 = unityPose.m12; cvPose.m23 = unityPose.m13;
            cvPose.m30 = 0; cvPose.m31 = 0; cvPose.m32 = 0; cvPose.m33 = 1;
            return cvPose;
        }
        public static void ConvertUnityPoseToCvPose_c2w(Matrix4x4 unityPose, out float[] poses)
        {
            Matrix4x4 cvPose = ConvertUnityPoseToCvPose_c2w(unityPose);
            poses = new float[16]; // initialize the array
                                   //Matrix4x4 to float array by row
            poses[0] = cvPose.m00; poses[1] = cvPose.m01; poses[2] = cvPose.m02; poses[3] = cvPose.m03;
            poses[4] = cvPose.m10; poses[5] = cvPose.m11; poses[6] = cvPose.m12; poses[7] = cvPose.m13;
            poses[8] = cvPose.m20; poses[9] = cvPose.m21; poses[10] = cvPose.m22; poses[11] = cvPose.m23;
            poses[12] = cvPose.m30; poses[13] = cvPose.m31; poses[14] = cvPose.m32; poses[15] = cvPose.m33;

        }

        public static Matrix4x4 GetMatrixFromLandscapeLeft(ScreenOrientation screenOrientation, Matrix4x4 cameraTransform)
        {
            Matrix4x4 rotateMatrix = Matrix4x4.identity;
            if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
            {
                switch (screenOrientation)
                {
                    case ScreenOrientation.Portrait:
                        rotateMatrix.SetRow(0, new Vector4(0, -1, 0, 0));
                        rotateMatrix.SetRow(1, new Vector4(1, 0, 0, 0));
                        break;
                    case ScreenOrientation.PortraitUpsideDown:
                        rotateMatrix.SetRow(0, new Vector4(0, 1, 0, 0));
                        rotateMatrix.SetRow(1, new Vector4(-1, 0, 0, 0));
                        break;
                    case ScreenOrientation.LandscapeLeft:
                        break;
                    case ScreenOrientation.LandscapeRight:
                        rotateMatrix.SetRow(0, new Vector4(-1, 0, 0, 0));
                        rotateMatrix.SetRow(1, new Vector4(0, -1, 0, 0));
                        break;
                    default:
                        break;
                }
            }
            //Debug.Log("GetMatrixFromLandscapeLeft rotateMatrix: " + rotateMatrix + " cameraTransform: " + screenOrientation);
            return cameraTransform * rotateMatrix;
        }

        public static Matrix4x4 GetMatrixToLandscapeLeft(ScreenOrientation screenOrientation, Matrix4x4 cameraTransform)
        {
            Matrix4x4 rotateMatrix = Matrix4x4.identity;
            if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
            {
                switch (screenOrientation)
                {
                    case ScreenOrientation.Portrait:
                        rotateMatrix.SetRow(0, new Vector4(0, 1, 0, 0));
                        rotateMatrix.SetRow(1, new Vector4(-1, 0, 0, 0));
                        break;
                    case ScreenOrientation.PortraitUpsideDown:
                        rotateMatrix.SetRow(0, new Vector4(0, -1, 0, 0));
                        rotateMatrix.SetRow(1, new Vector4(1, 0, 0, 0));
                        break;
                    case ScreenOrientation.LandscapeLeft:
                        break;
                    case ScreenOrientation.LandscapeRight:
                        rotateMatrix.SetRow(0, new Vector4(-1, 0, 0, 0));
                        rotateMatrix.SetRow(1, new Vector4(0, -1, 0, 0));
                        break;
                    default:
                        break;
                }
            }

            //Debug.Log("GetMatrixToLandscapeLeft rotateMatrix: " + rotateMatrix + " cameraTransform: " + screenOrientation);
            return cameraTransform * rotateMatrix;
        }

        public static Matrix4x4 ConvertUnityPoseToCVPose_c2c(Matrix4x4 posein_unity)
        {
            Matrix4x4 poseout_cv = Matrix4x4.identity;

            Matrix4x4 left = Matrix4x4.identity;
            {
                left.m11 = -1;  // finaly left = [1, -1, 1, 1]
            } 
            Matrix4x4 right = Matrix4x4.identity;
            {
                right.m11 = -1; // finaly right = [1, -1, 1, 1]
            }
            poseout_cv = left * posein_unity * right;

            return poseout_cv;
        }

        public static Matrix4x4 ConvertImagePoseToUnityPose_c2w(Matrix4x4 cvPose)
        {
            Matrix4x4 unityPose = Matrix4x4.identity;
            unityPose.m00 = cvPose.m00; unityPose.m01 = -cvPose.m01; unityPose.m02 = cvPose.m02; unityPose.m03 = cvPose.m03;
            unityPose.m10 = -cvPose.m20; unityPose.m11 = cvPose.m21; unityPose.m12 = -cvPose.m22; unityPose.m13 = -cvPose.m23;
            unityPose.m20 = -cvPose.m10; unityPose.m21 = cvPose.m11; unityPose.m22 = -cvPose.m12; unityPose.m23 = -cvPose.m13;
            unityPose.m30 = 0; unityPose.m31 = 0; unityPose.m32 = 0; unityPose.m33 = 1;
            return unityPose;
        }

        public static Matrix4x4 ConvertImagePoseToUnityPose_c2w(float[] poses)
        {
            Matrix4x4 cvPose = Matrix4x4.identity;
            cvPose.m00 = poses[0]; cvPose.m01 = poses[1]; cvPose.m02 = poses[2]; cvPose.m03 = poses[3];
            cvPose.m10 = poses[4]; cvPose.m11 = poses[5]; cvPose.m12 = poses[6]; cvPose.m13 = poses[7];
            cvPose.m20 = poses[8]; cvPose.m21 = poses[9]; cvPose.m22 = poses[10]; cvPose.m23 = poses[11];
            cvPose.m30 = poses[12]; cvPose.m31 = poses[13]; cvPose.m32 = poses[14]; cvPose.m33 = poses[15];

            Matrix4x4 unityPose = ConvertImagePoseToUnityPose_c2w(cvPose);
            return unityPose;
        }
    }
}
