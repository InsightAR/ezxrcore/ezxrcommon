﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace EzxrCore.Common
{
/// <summary>
/// Json转换工具类
/// </summary>
public static class JsonUtil
{
    [Serializable]
    public class Serialization<T>
    {
        [SerializeField]
        List<T> target;
        public List<T> ToList() { return target; }

        public Serialization(List<T> target)
        {
            this.target = target;
        }
    }

	///// <summary> 把对象转换为Json字符串 </summary>
	///// <param name="obj">对象</param>
	//public static string ToJson<T>(T obj)
	//{
	//	return JsonUtility.ToJson(obj);
	//}

    /// <summary> 解析Json数组 </summary>
    /// <typeparam name="T">类型</typeparam>
    /// <param name="json">Json字符串</param>
    public static List<T> FromJsonArray<T>(string json)
    {
        if (json == "null" && typeof(T).IsClass) return null;

        json = "{\"data\":{data}}".Replace("{data}", json);
        
        Pack<T> Pack = JsonUtility.FromJson<Pack<T>>(json);

        return Pack.data;
    }

    /// <summary>
    /// 解析Json，若为数组形式则返回首个元素
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="json"></param>
    /// <returns></returns>
    public static T FromJson<T>(string json)
    {
        try
        {
            return JsonUtility.FromJson<T>(json);
        }
        catch (System.Exception ex)
        {
            Debug.LogError("JsonUtil.FromJson<T> Error: " + ex.Message);
            List<T> list = FromJsonArray<T>(json);
            if (list == null || list.Count < 1)
            {
                return default;
            }
            return list[0];
        }
    }

    public static string ToJson<T>(T pdata)
    {
        return JsonUtility.ToJson(pdata);
    }

	/// <summary> 内部包装类 </summary>
	public class Pack<T>
	{
		public List<T> data;
	}
}
}