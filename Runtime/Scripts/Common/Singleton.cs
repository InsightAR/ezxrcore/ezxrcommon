﻿namespace EzxrCore.Common
{
public class Singleton<T> where T : new()
{
    public static T _instance;
    private static readonly object _lock = new object();

    public static T Instance
    {
        get
        {
            if (_instance == null)
            {
                lock (_lock)
                {
                    _instance = new T();
                }
            }
            return _instance;
        }
    }

    public virtual void Initialize()
    {

    }

    public virtual void UnInitialize()
    {

    }
}
}
